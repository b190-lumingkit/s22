/*
    Create functions which can manipulate our arrays.
*/

let registeredUsers = [

    "James Jeffries",
    "Gunther Smith",
    "Macie West",
    "Michelle Queen",
    "Shane Miguelito",
    "Fernando Dela Cruz",
    "Akiko Yukihime"
];

let friendsList = [];
//added new variable
let currentUser = "";

/*
    
   1. Create a function which will allow us to register into the registeredUsers list.
        - this function should be able to receive a string.
        - determine if the input username already exists in our registeredUsers array.
            -if it is, show an alert window with the following message:
                "Registration failed. Username already exists!"
            -if it is not, add the new username into the registeredUsers array and show an alert:
                "Thank you for registering!"
        - invoke and register a new user.
        - outside the function log the registeredUsers array.

*/
function register(user) {
    //check if user is already registered, if not register user
    if (currentUser !== "") {
        return "You are already registered in this session. Refresh browser if you want to register again.";
    } else {
        if (registeredUsers.includes(user)) {
            alert("Registration failed. Username already exists!");
            return "Registration failed."
        } else {
            registeredUsers.push(user);
            currentUser = user;
            alert("Thank you for registering!");
            return "Registration successful.";
        }
    }
}
    
/*
    2. Create a function which will allow us to add a registered user into our friends list.
        - this function should be able to receive a string.
        - determine if the input username exists in our registeredUsers array.
            - if it is, add the foundUser in our friendList array.
                    -Then show an alert with the following message:
                        - "You have added <registeredUser> as a friend!"
            - if it is not, show an alert window with the following message:
                - "User not found."
        - invoke the function and add a registered user in your friendsList.
        - Outside the function log the friendsList array in the console.

*/
function addFriend(username) {
    //check first if user is registered. if not, don't allow user to addFriend
    if (currentUser !== "") {
        if (registeredUsers.includes(username)) {
            friendsList.push(username);
            alert(`You have added ${username} as a friend.`)
            return username + " added as a friend.";
        } else {
            alert("User not found.");
            return "User not found.";
        }
    }
     else {
        return "You cannot add a friend if you're not registered. Please register.";
    }
}

/*
    3. Create a function which will allow us to show/display the items in the friendList one by one on our console.
        - If the friendsList is empty show an alert: 
            - "You currently have 0 friends. Add one first."
        - Invoke the function.

*/
function showFriends() {
    if (currentUser !== "") {
        if (friendsList.length === 0) {
            alert("You currently have 0 friends. Add one first.")
            return "You have 0 friends atm :< Add one first.";
        } else {
            console.log("Your friends are: ")
            return friendsList.forEach(friend => console.log(friend));
        }
    }
     else {
        return "You cannot see your friends if you're not registered. Please register.";
    }
}


/*
    4. Create a function which will display the amount of registered users in your friendsList.
        - If the friendsList is empty show an alert:
            - "You currently have 0 friends. Add one first."
        - If the friendsList is not empty show an alert:
            - "You currently have <numberOfFriends> friends."
        - Invoke the function

*/

function countFriends() {
    //check first if user is registered. if not, don't allow user to countFriends
    if (currentUser !== "") {
        let noFriends = "You currently have 0 friends. Add one first.";
        let haveFriends = `You currently have ${friendsList.length} friends`;
        alert((friendsList === 0) ? noFriends : haveFriends);
        return haveFriends;
    }
     else {
        return "You cannot count your friends if you're not registered. Please register.";
    }
}

/*
    5. Create a function which will delete the last registeredUser you have added in the friendsList.
        - If the friendsList is empty show an alert:
            - "You currently have 0 friends. Add one first."
        - Invoke the function.
        - Outside the function log the friendsList array.

*/
/*
    Stretch Goal:

    Instead of only deleting the last registered user in the friendsList delete a specific user instead.
        -You may get the user's index.
        -Then delete the specific user with splice().

*/
function removeFriend(friendIndex) {
    if (currentUser !== "") {
        if (friendsList === 0) {
            alert("You currently have 0 friends. Add one first.");
            return "You have 0 friends to remove.";
        } else {
            //check if friendIndex is a number or if friendIndex exists
            if (typeof friendIndex !== 'number' || friendIndex > friendsList.length) {
                return "Invalid input. Must be a number or index does not exist.";
            } else {
                deletedFriend = friendsList[friendIndex];
                //default remove count is 1 to prevent other friends from getting removed
                friendsList.splice(friendIndex, 1);
                console.log(`You have removed ${deletedFriend} from your friends list.`);
                return `Current friends: ${friendsList.join(", ")}`;
            }
        }
    }
     else {
        return "You cannot delete your friend if you're not registered. Please register.";
    }
}



